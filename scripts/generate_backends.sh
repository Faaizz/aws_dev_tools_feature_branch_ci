#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd "${SCRIPT_DIR}" || exit

test -d ./.venv || python -m venv ./.venv
source ./.venv/bin/activate
pip install jinja2-cli==0.8.2 Jinja2==3.1.2

AWS_ACCOUNT_ID=$(aws sts get-caller-identity --query 'Account' --output text)
REGION=$(aws configure get region || echo $AWS_REGION)
S3_BUCKET="terraform-state-${AWS_ACCOUNT_ID}-${REGION}"
DYNAMODB_TABLE="terraform-state-lock"

for file in $(find .. -type f -name "*backend.jinja")
do
  jinja2 "${file}" \
    -D REGION="${REGION}" \
    -D S3_BUCKET="${S3_BUCKET}" \
    -D DYNAMODB_TABLE="${DYNAMODB_TABLE}" > "${file%.jinja}.tf"
done
