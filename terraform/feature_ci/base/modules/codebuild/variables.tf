variable "name" {
  type        = string
  description = "The name of the CodeBuild project."
}

variable "description" {
  type        = string
  description = "The description of the CodeBuild project."
  default     = ""
}

variable "source_path" {
  type        = string
  description = "The path to the source code in the CodeCommit repository."
}
variable "artifact_s3_bucket_id" {
  type        = string
  description = "The ID of the S3 bucket where the CodeBuild artifacts will be fetched/stored."
}

variable "artifact_kms_key_arn" {
  type        = string
  description = "The ARN of the KMS key used to encrypt/decrypt the CodeBuild artifacts."
}

variable "compliance_path" {
  type        = string
  description = "Path to terraform-compliance feature files."
}

variable "envs" {
  type        = map(string)
  description = "Environment variables to be passed to the CodeBuild project."
  default     = {}
}
