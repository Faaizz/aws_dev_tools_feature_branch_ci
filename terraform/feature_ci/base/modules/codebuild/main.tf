resource "aws_codebuild_project" "this" {
  name        = var.name
  description = var.description

  service_role = aws_iam_role.service_role.arn

  artifacts {
    type = "CODEPIPELINE"
  }

  environment {
    compute_type = "BUILD_GENERAL1_SMALL"
    image        = "aws/codebuild/amazonlinux2-x86_64-standard:3.0"
    type         = "LINUX_CONTAINER"


    environment_variable {
      name  = "SOURCE_PATH"
      value = var.source_path
    }
    environment_variable {
      name  = "COMPLIANCE_PATH"
      value = var.compliance_path
    }
    environment_variable {
      name  = "AFT_ADMIN_ROLE_ARN"
      value = aws_iam_role.aft_execution.arn
    }

    dynamic "environment_variable" {
      for_each = var.envs
      content {
        name  = environment_variable.key
        value = environment_variable.value
      }
    }
  }

  source {
    type      = "CODEPIPELINE"
    buildspec = file("${abspath(path.module)}/files/buildspec/terraform-compliance.yml")
  }
}

resource "aws_iam_policy" "service_policy" {
  name_prefix = var.name
  description = "Allow access to CloudWatch Logs and ECR"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid    = "CWLogsAccess"
        Effect = "Allow"
        Action = [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents",
        ]
        Resource = "arn:aws:logs:*:*:*"
      },
      {
        Sid    = "S3Access"
        Effect = "Allow"
        Action = [
          "s3:GetObject",
          "s3:GetObjectVersion",
          "s3:GetBucketVersioning",
          "s3:PutObject",
          "s3:GetBucketAcl",
          "s3:GetEncryptionConfiguration",
        ]
        Resource = [
          "arn:aws:s3:::${var.artifact_s3_bucket_id}",
          "arn:aws:s3:::${var.artifact_s3_bucket_id}/*",
        ]
      },
      {
        Sid    = "KMSAccess"
        Effect = "Allow"
        Action = [
          "kms:Decrypt",
          "kms:Encrypt",
          "kms:ReEncrypt*",
          "kms:GenerateDataKey*",
          "kms:DescribeKey",
        ]
        Resource = var.artifact_kms_key_arn
      },
    ]
  })
}

resource "aws_iam_role" "service_role" {
  name_prefix = var.name
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = ["sts:AssumeRole"]
        Principal = {
          Service = "codebuild.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "service_role" {
  role       = aws_iam_role.service_role.name
  policy_arn = aws_iam_policy.service_policy.arn
}

resource "aws_iam_role" "aft_execution" {
  name        = "CustomAFTExecution"
  description = "Provide administrator access for terraform"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = ["sts:AssumeRole"]
        Principal = {
          AWS = aws_iam_role.service_role.arn
        }
      }
    ]
  })
  managed_policy_arns = [data.aws_iam_policy.administrator_access.arn]
}
