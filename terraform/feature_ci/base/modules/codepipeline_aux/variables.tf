variable "repo_name" {
  type        = string
  description = "The name of the CodeCommit repository to use."
}

variable "codebuild_projects_arns" {
  description = "CodeBuild project ARNs"
  type        = list(string)
  default     = []
}
