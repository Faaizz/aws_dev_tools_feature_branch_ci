module "kms" {
  source  = "terraform-aws-modules/kms/aws"
  version = "1.5.0"

  description = "Artifact encryption key for CodePipeline"
  key_usage   = "ENCRYPT_DECRYPT"

  key_owners = [
    data.aws_caller_identity.current.arn,
  ]
  key_service_users = [
    aws_iam_role.this.arn,
  ]

  aliases = ["feature-ci/codepipeline"]
}

module "s3" {
  source  = "terraform-aws-modules/s3-bucket/aws"
  version = "3.8.2"

  bucket        = "codepipeline-artifacts-${data.aws_caller_identity.current.account_id}-${data.aws_region.current.name}"
  force_destroy = true

  server_side_encryption_configuration = {
    rule = {
      apply_server_side_encryption_by_default = {
        kms_master_key_id = module.kms.key_id
        sse_algorithm     = "aws:kms"
      }
    }
  }
}

resource "aws_iam_policy" "this" {
  name_prefix = "CoadePipelineServiceRolePolicy-"
  description = "Allow CodePipeline to access resources"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid    = "CodeCommitAccess"
        Effect = "Allow"
        Action = [
          "codecommit:GetBranch",
          "codecommit:GetRepository",
          "codecommit:GetCommit",
          "codecommit:UploadArchive",
          "codecommit:GetUploadArchiveStatus",
          "codecommit:CancelUploadArchive",
        ]
        Resource = "arn:aws:codecommit:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${var.repo_name}"
      },
      {
        Sid    = "CodeBuildAccess"
        Effect = "Allow"
        Action = [
          "codebuild:BatchGetBuilds",
          "codebuild:StartBuild",
        ]
        Resource = var.codebuild_projects_arns
      },
      {
        Sid    = "S3Access"
        Effect = "Allow"
        Action = [
          "s3:GetObject",
          "s3:GetObjectVersion",
          "s3:GetBucketVersioning",
          "s3:PutObject",
          "s3:GetBucketAcl",
          "s3:GetEncryptionConfiguration",
        ]
        Resource = [
          "arn:aws:s3:::${module.s3.s3_bucket_id}",
          "arn:aws:s3:::${module.s3.s3_bucket_id}/*",
        ]
      },
      {
        Sid    = "KMSAccess"
        Effect = "Allow"
        Action = [
          "kms:Decrypt",
          "kms:Encrypt",
          "kms:ReEncrypt*",
          "kms:GenerateDataKey*",
          "kms:DescribeKey",
        ]
        Resource = module.kms.key_arn
      },
      {
        Sid    = "CloudWatchLogsAccess"
        Effect = "Allow"
        Action = [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents",
        ]
        Resource = "arn:aws:logs:*:*:*"
      },
    ]
  })
}

resource "aws_iam_role" "this" {
  name_prefix = "CodePipelineServiceRole-"
  description = "CodePipeline service role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = ["sts:AssumeRole"]
        Principal = {
          Service = "codepipeline.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "this" {
  role       = aws_iam_role.this.name
  policy_arn = aws_iam_policy.this.arn
}

resource "aws_s3_object" "pipeline_template" {
  bucket = module.s3.s3_bucket_id
  key    = "pipeline.yml"
  source = "${path.module}/files/pipeline.yml"

  etag = filemd5("${path.module}/files/pipeline.yml")
}
