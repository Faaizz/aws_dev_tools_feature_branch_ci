output "kms_arn" {
  description = "ARN of the KMS key for CodePipeline"
  value       = module.kms.key_arn
}

output "bucket_id" {
  description = "Name of the S3 bucket for CodePipeline"
  value       = module.s3.s3_bucket_id
}

output "role_arn" {
  description = "ARN of the IAM role for CodePipeline"
  value       = aws_iam_role.this.arn
}

output "codepipeline_cfn_template_url" {
  description = "Key of the CodePipeline CloudFormation deployment template"
  value       = "https://s3.amazonaws.com/${module.s3.s3_bucket_id}/${aws_s3_object.pipeline_template.key}"
}
