module "lambda" {
  source  = "terraform-aws-modules/lambda/aws"
  version = "4.16.0"

  function_name          = "lambda-trigger-pipeline-${var.codecommit_repo_name}"
  description            = "Lambda function to create and trigger the CI pipeline"
  handler                = "main.lambda_handler"
  runtime                = "python3.8"
  architectures          = ["arm64"]
  publish                = true
  timeout                = 600
  memory_size            = 1024
  ephemeral_storage_size = 2048

  source_path = "${path.module}/files/src"

  environment_variables = {
    SOURCE_BUCKET_NAME                    = var.artifact_bucket
    CODEPIPELINE_ARTIFACTS_BUCKET_KEY_ARN = var.artifact_bucket_kms_key_arn
    CODEPIPELINE_ROLE_ARN                 = var.codepipeline_role_arn
    CODEBUILD_COMPLIANCE                  = var.compliance_codebuild
    TEMPLATE_URL                          = var.template_url
  }

  allowed_triggers = {
    EventBridge = {
      principal  = "events.amazonaws.com"
      source_arn = aws_cloudwatch_event_rule.trigger_pipeline.arn
    }
  }

  attach_policy_json = true
  policy_json = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid      = "CodePipelineAccess"
        Effect   = "Allow"
        Action   = ["codepipeline:*"]
        Resource = "*"
      },
      {
        Sid    = "S3AccessArtifactBucket"
        Effect = "Allow"
        Action = [
          "s3:GetObject",
        ]
        Resource = [
          "arn:aws:s3:::${var.artifact_bucket}/*",
        ]
      },
      {
        Sid    = "AccessArtifactKMSAccess"
        Effect = "Allow"
        Action = [
          "kms:Decrypt",
          "kms:Encrypt",
          "kms:ReEncrypt*",
          "kms:GenerateDataKey*",
          "kms:DescribeKey",
        ]
        Resource = var.artifact_bucket_kms_key_arn
      },
      {
        Sid    = "CFN"
        Effect = "Allow",
        Action = [
          "cloudformation:CreateStack",
          "cloudformation:DescribeStacks",
          "cloudformation:DescribeStackEvents",
          "cloudformation:DescribeStackResources",
          "cloudformation:GetTemplate",
          "cloudformation:ValidateTemplate",
          "cloudformation:DeleteStack",
          "cloudformation:UpdateStack",
        ],
        Resource = "*"
      },
      {
        Sid    = "IAMPassRoleOnCodePipelineServiceRole"
        Effect = "Allow",
        Action = [
          "iam:PassRole",
        ],
        Resource = [var.codepipeline_role_arn]
      }
    ]
  })
}

resource "aws_cloudwatch_event_rule" "trigger_pipeline" {
  name        = "trigger-pipeline-${var.codecommit_repo_name}"
  description = "Trigger lambda to create the CI pipeline"

  event_pattern = jsonencode({
    source = [
      "aws.codecommit"
    ]
    detail-type = [
      "CodeCommit Repository State Change"
    ]
    detail = {
      event = [
        "referenceCreated",
        "referenceDeleted",
      ]
      referenceType = [
        "branch"
      ]
    }
  })
}

resource "aws_cloudwatch_event_target" "trigger_pipeline" {
  arn  = module.lambda.lambda_function_arn
  rule = aws_cloudwatch_event_rule.trigger_pipeline.id
}
