import os
import boto3

SOURCE_BUCKET_NAME = os.environ['SOURCE_BUCKET_NAME']
CODEPIPELINE_ARTIFACTS_BUCKET_KEY_ARN= os.environ['CODEPIPELINE_ARTIFACTS_BUCKET_KEY_ARN']
CODEPIPELINE_ROLE_ARN = os.environ['CODEPIPELINE_ROLE_ARN']
CODEBUILD_COMPLIANCE = os.environ['CODEBUILD_COMPLIANCE']
TEMPLATE_URL = os.environ['TEMPLATE_URL']

s3_client = boto3.client('s3')
cf_client = boto3.client('cloudformation')

def lambda_handler(event, _):
  repository_name = event['detail']['repositoryName']
  branch_name = event['detail']['referenceName']
  filtered_branch_name = filter_branch_name(branch_name)

  event_type = event['detail']['event']
  
  print(f'REPO NAME: {repository_name}')
  print(f'BRANCH NAME: {branch_name}')
  print(f'EVENT TYPE: {event_type}')

  if branch_name in ['master', 'main']:
    quit()


  if event_type == 'referenceCreated':
    print(f'creating pipeline for {branch_name}')
    cf_client.create_stack(
      StackName=f'Pipeline-{repository_name}-{filtered_branch_name}',
      TemplateURL=TEMPLATE_URL,
      Parameters=[
        {
            'ParameterKey': 'Name',
            'ParameterValue': f'feature-{repository_name}-{filtered_branch_name}',
            'UsePreviousValue': False
        },
        {
            'ParameterKey': 'RepoName',
            'ParameterValue': repository_name,
            'UsePreviousValue': False
        },
        {
            'ParameterKey': 'RepoBranch',
            'ParameterValue': branch_name,
            'UsePreviousValue': False
        },
        {
            'ParameterKey': 'ArtifactBucket',
            'ParameterValue': SOURCE_BUCKET_NAME,
            'UsePreviousValue': False
        },
        {
            'ParameterKey': 'ArtifactBucketKMSKeyARN',
            'ParameterValue': CODEPIPELINE_ARTIFACTS_BUCKET_KEY_ARN,
            'UsePreviousValue': False
        },
        {
            'ParameterKey': 'CodePipelineRoleARN',
            'ParameterValue': CODEPIPELINE_ROLE_ARN,
            'UsePreviousValue': False
        },
        {
            'ParameterKey': 'ComplianceCodeBuildProject',
            'ParameterValue': CODEBUILD_COMPLIANCE,
            'UsePreviousValue': False
        },
      ],
      OnFailure='ROLLBACK',
      Capabilities=['CAPABILITY_NAMED_IAM']
    )
    print(f'created pipeline for {branch_name}')
  else:
    print(f'deleting pipeline for {branch_name}')
    cf_client.delete_stack(
      StackName=f'Pipeline-{repository_name}-{filtered_branch_name}'
    )
    print(f'deleted pipeline for {branch_name}')

def filter_branch_name(branch_name: str) -> str:
  new_branch_name = ''.join([c for c in branch_name if c.isalnum() or c == '-'])
  return new_branch_name
