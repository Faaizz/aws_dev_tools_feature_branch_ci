variable "artifact_bucket" {
  type        = string
  description = "The name of the S3 bucket to use for storing artifacts. Also holds the CodePipeline source code."
}

variable "artifact_bucket_kms_key_arn" {
  type        = string
  description = "The ARN of the KMS key to use for encrypting artifacts."
}

variable "codepipeline_role_arn" {
  type        = string
  description = "The ARN of the IAM role to use for CodePipeline."
}

variable "compliance_codebuild" {
  type        = string
  description = "Validation CodeBuild project."
}

variable "codecommit_repo_name" {
  description = "Name of CodeCommit repository on which to trigger pipelines"
  type        = string
}

variable "template_url" {
  type        = string
  description = "URL of the CodePipeline CloudFormation deployment template"
}
