terraform {
  required_version = "~> 1.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.63.0"
    }
  }

  backend "s3" {
    region         = "eu-central-1"
    bucket         = "tf-state-318907891627"
    dynamodb_table = "terraform-lock"
    key            = "feature-branch-ci-aft-account-request/base.tfstate"
  }
}
