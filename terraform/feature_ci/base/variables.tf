variable "repo_name" {
  type        = string
  description = "The name of the CodeCommit repository to use."
}

variable "source_path" {
  type        = string
  description = "The path to the source code in the CodeCommit repository."
}

variable "compliance_path" {
  type        = string
  description = "Path to terraform-compliance feature files."
}
variable "compliance_codebuild_envs" {
  type        = map(string)
  description = "Environment variables to be passed to the compliance CodeBuild project."
  default     = {}
}
