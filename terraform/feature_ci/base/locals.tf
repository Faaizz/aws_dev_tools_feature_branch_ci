locals {
  codebuild = {
    compliance = {
      name        = "TerraformCompliance"
      description = "Terraform Compliance CodeBuild project"
    }
  }
}
