output "compliance_codebuild" {
  description = "CodeBuild project name for validation stage"
  value       = module.compliance_codebuild.project_name
}

output "kms_codepipeline_arn" {
  description = "ARN of the KMS key for CodePipeline"
  value       = module.codepipeline_aux.kms_arn
}

output "s3_codepipeline_bucket" {
  description = "Name of the S3 bucket for CodePipeline"
  value       = module.codepipeline_aux.bucket_id
}

output "codepipeline_role_arn" {
  description = "ARN of the IAM role for CodePipeline"
  value       = module.codepipeline_aux.role_arn
}
