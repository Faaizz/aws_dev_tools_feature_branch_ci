repo_name   = "BaseRepository"
source_path = "terraform"

compliance_path = "features"
compliance_codebuild_envs = {
  "PROVIDER_REGION" = "eu-central-1"
  "BACKEND_REGION"  = "eu-central-1"
  "BACKEND_BUCKET"  = "aft-backend-12345678910-primary-region"
  "BACKEND_KEY"     = "tmp/terraform.tfstate"
  "DYNAMODB_TABLE"  = "aft-backend-12345678910-primary-region"
  "KMS_KEY_ID"      = "abcdefhi-2511-0000-0123-43a8ee019395"
}
