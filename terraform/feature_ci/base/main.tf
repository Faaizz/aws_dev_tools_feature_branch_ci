module "compliance_codebuild" {
  source = "./modules/codebuild"

  name        = local.codebuild.compliance.name
  description = local.codebuild.compliance.description

  source_path     = var.source_path
  compliance_path = var.compliance_path

  artifact_s3_bucket_id = module.codepipeline_aux.bucket_id
  artifact_kms_key_arn  = module.codepipeline_aux.kms_arn

  envs = var.compliance_codebuild_envs
}

module "codepipeline_aux" {
  source = "./modules/codepipeline_aux"

  repo_name               = var.repo_name
  codebuild_projects_arns = ["arn:aws:codebuild:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:project/${local.codebuild.compliance.name}"]
}

module "lambda_trigger" {
  source = "./modules/lambda_trigger"

  artifact_bucket             = module.codepipeline_aux.bucket_id
  artifact_bucket_kms_key_arn = module.codepipeline_aux.kms_arn
  codepipeline_role_arn       = module.codepipeline_aux.role_arn
  codecommit_repo_name        = var.repo_name
  compliance_codebuild        = module.compliance_codebuild.project_name
  template_url                = module.codepipeline_aux.codepipeline_cfn_template_url
}
