provider "aws" {
  default_tags {
    tags = {
      ManagedBy = "Terraform"
      State     = "Local"
      Gorup     = "Pre-Deployment"
    }
  }
}
