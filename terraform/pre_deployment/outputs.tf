output "s3_backend" {
  description = "S3 bucket for Terraform state"
  value       = module.s3_backend.s3_bucket_id
}

output "state_lock_table" {
  description = "DynamoDB table for Terraform state lock"
  value       = module.state_lock_table.dynamodb_table_id
}

output "repository_name" {
  description = "Name of the CodeCommit repository"
  value       = aws_codecommit_repository.base.repository_name
}
