resource "aws_codecommit_repository" "base" {
  repository_name = "BaseRepository"
  description     = "This is the base repository"
}

module "s3_backend" {
  source  = "terraform-aws-modules/s3-bucket/aws"
  version = "3.8.2"

  bucket        = "terraform-state-${data.aws_caller_identity.current.account_id}-${data.aws_region.current.name}"
  force_destroy = true
}

module "state_lock_table" {
  source  = "terraform-aws-modules/dynamodb-table/aws"
  version = "3.2.0"

  name     = "terraform-state-lock"
  hash_key = "LockID"

  attributes = [
    {
      name = "LockID"
      type = "S"
    }
  ]
}
